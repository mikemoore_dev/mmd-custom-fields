window.onload = function(){
	
	var mlmURL = window.location.href;

	if(mlmURL.indexOf("mmd_customfields_content") > -1){

		var imgs = document.getElementsByClassName("small-image-preview");

		for (var i = 0; i < imgs.length; i++) {
		    
			if(typeof imgs[i] !== "undefined"){
		    	
		    	var src = imgs[i].getAttribute("src");
		    		src = src.replace("media/", "media/mmdcustomfields/");
		    		imgs[i].setAttribute("src", src);

		    	var a = imgs[i].parentNode;
		    	var href = a.getAttribute("href");
		    		href = href.replace("media/", "media/mmdcustomfields/");
		    		a.setAttribute("href", href);

		    }

		}
	}

};