<?php
 
class Mmd_CustomFields_Model_Observer
{
    
    public function mmd_customfields_add(Varien_Event_Observer $observer)
    {

        $helper = Mage::helper('mmd_customfields');

        $config 	= $observer->getConfig();
        $cmsPages 	= Mage::getModel('cms/page')->getCollection();

        $GROUP_XML  = '';
        $adminSectionGroups = $config->getNode('sections/mmd_customfields_add/groups');

        $GROUP_XML = '<mmd_cf_add_global>
                        <label>Global (Header/Footer)</label>
                        <sort_order>0</sort_order>
                        <show_in_default>1</show_in_default>
                        <show_in_website>1</show_in_website>
                        <show_in_store>1</show_in_store>
                        <fields>
                            <text_fields_0>
                                <label>Textboxes</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>1</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </text_fields_0>
                            <textarea_fields_0>
                                <label>Textareas</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>2</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </textarea_fields_0>
                            <wysiwyg_fields_0>
                                <label>WYSIWYG</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>3</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </wysiwyg_fields_0>
                            <image_fields_0>
                                <label>Image Uploads</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>4</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </image_fields_0>
                            <yn_fields_0>
                                <label>Yes/No</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>5</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </yn_fields_0>
                            <ed_fields_0>
                                <label>Enable/Disable</label>
                                <frontend_type>text</frontend_type>
                                <validate>validate-zero-or-greater</validate>
                                <sort_order>6</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </ed_fields_0>
                            <fields_note_0>
                                <frontend_type>note</frontend_type>
                                <comment>
                                    <![CDATA[Specify a number in the boxes above to create custom content fields for the <span class="notice">header and footer</span>.]]>
                                </comment>
                                <sort_order>7</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                            </fields_note_0>
                        </fields>
                    </mmd_cf_add_global>';

        $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
        $adminSectionGroups->appendChild($new_group_xml);

    	$i = 1;
        $x = 0;
    	foreach($cmsPages as $p){

            $pageId     = $p->getData('page_id');
            $pageTitle  = $p->getData('title');
            $pageAlias  = $helper->aliasify($pageTitle);

            $GROUP_XML = '<mmd_cf_add_'.$pageAlias.'>
                                <label>'.$pageTitle.'</label>
                                <sort_order>'.$i.'</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                                <fields>
                                    <text_fields_'.$i.'>
        		                        <label>Textboxes</label>
        		                        <frontend_type>text</frontend_type>
        		                        <validate>validate-zero-or-greater</validate>
        		                        <sort_order>'.$x++.'</sort_order>
        		                        <show_in_default>1</show_in_default>
        		                        <show_in_website>1</show_in_website>
        		                        <show_in_store>1</show_in_store>
        		                    </text_fields_'.$i.'>
                                    <textarea_fields_'.$i.'>
                                        <label>Textareas</label>
                                        <frontend_type>text</frontend_type>
                                        <validate>validate-zero-or-greater</validate>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </textarea_fields_'.$i.'>
                                    <wysiwyg_fields_'.$i.'>
                                        <label>WYSIWYG</label>
                                        <frontend_type>text</frontend_type>
                                        <validate>validate-zero-or-greater</validate>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </wysiwyg_fields_'.$i.'>
                                    <image_fields_'.$i.'>
                                        <label>Image Uploads</label>
                                        <frontend_type>text</frontend_type>
                                        <validate>validate-zero-or-greater</validate>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </image_fields_'.$i.'>
                                    <yn_fields_'.$i.'>
                                        <label>Yes/No</label>
                                        <frontend_type>text</frontend_type>
                                        <validate>validate-zero-or-greater</validate>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </yn_fields_'.$i.'>
                                    <ed_fields_'.$i.'>
                                        <label>Enable/Disable</label>
                                        <frontend_type>text</frontend_type>
                                        <validate>validate-zero-or-greater</validate>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </ed_fields_'.$i.'>
                                    <fields_note_'.$i.'>
                                        <frontend_type>note</frontend_type>
                                        <comment>
                                            <![CDATA[Specify a number in the boxes above to create custom content fields for the <span class="notice">'.$pageTitle.'</span> page.]]>
                                        </comment>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </fields_note_'.$i.'>
                                </fields>
                            </mmd_cf_add_'.$pageAlias.'>';

            $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
            $adminSectionGroups->appendChild($new_group_xml);

            $i++; 	
    	}        
 
        return $this;
    }

    public function mmd_customfields_name(Varien_Event_Observer $observer)
    {

        $helper = Mage::helper('mmd_customfields');

        $config     = $observer->getConfig();
        $cmsPages   = Mage::getModel('cms/page')->getCollection();

        $fieldTypes = array(
            'text_fields_'      => 'Textbox',
            'textarea_fields_'  => 'Textarea',
            'wysiwyg_fields_'   => 'WYSIWYG',
            'image_fields_'     => 'Image',
            'yn_fields_'        => 'Yes/No',
            'ed_fields_'        => 'Enable/Disable'
        );

        $GROUP_XML  = '';

        $adminSectionGroups = $config->getNode('sections/mmd_customfields_name/groups');

        $GROUP_XML = '<mmd_cf_name_global>
                        <label>Global (Header/Footer)</label>
                        <sort_order>0</sort_order>
                        <show_in_default>1</show_in_default>
                        <show_in_website>1</show_in_website>
                        <show_in_store>1</show_in_store>
                        <fields>';
        $x = 0;           
        foreach($fieldTypes as $k => $fieldType){

            $fieldCount = Mage::getStoreConfig('mmd_customfields_add/mmd_cf_add_global/'.$k.'0');

            for($fields=1;$fields<=$fieldCount;$fields++){

                $GROUP_XML .= '<field_global_'.$k.$fields.'>
                                    <label>'.$fieldType.' '.$fields.'</label>
                                    <frontend_type>text</frontend_type>
                                    <sort_order>'.$x.'</sort_order>
                                    <show_in_default>1</show_in_default>
                                    <show_in_website>1</show_in_website>
                                    <show_in_store>1</show_in_store>
                                </field_global_'.$k.$fields.'>';

                $x++;
            }
            
            if(isset($fieldCount)){
                if(intval($fieldCount)!=0){
                    $GROUP_XML .= '<fields_note_'.$k.'0>
                                        <frontend_type>note</frontend_type>
                                        <comment>
                                            <![CDATA[Enter names for each of the custom <span class="notice">'.$fieldType.'</span> fields.]]>
                                        </comment>
                                        <sort_order>'.$x++.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </fields_note_'.$k.'0>';
                }
            }


        }

        $GROUP_XML .= '</fields>
            </mmd_cf_name_global>';

        $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
        $adminSectionGroups->appendChild($new_group_xml);

        $i = 1;
        $x = 0;
        foreach($cmsPages as $p){

            $pageId     = $p->getData('page_id');
            $pageTitle  = $p->getData('title');
            $pageAlias  = $helper->aliasify($pageTitle);

            $GROUP_XML = '<mmd_cf_name_'.$pageAlias.'>
                                <label>'.$pageTitle.'</label>
                                <sort_order>'.$i.'</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                                <fields>';
                                    
                    foreach($fieldTypes as $k => $fieldType){

                        $fieldCount = Mage::getStoreConfig('mmd_customfields_add/mmd_cf_add_'.$pageAlias.'/'.$k.$i);

                        for($fields=1;$fields<=$fieldCount;$fields++){

                            $GROUP_XML .= '<field_'.$pageAlias.'_'.$k.$fields.'>
                                                <label>'.$fieldType.' '.$fields.'</label>
                                                <frontend_type>text</frontend_type>
                                                <sort_order>'.$x.'</sort_order>
                                                <show_in_default>1</show_in_default>
                                                <show_in_website>1</show_in_website>
                                                <show_in_store>1</show_in_store>
                                            </field_'.$pageAlias.'_'.$k.$fields.'>';

                            $x++;
                        }
                        
                        if(isset($fieldCount)){
                            if(intval($fieldCount)!=0){
                                $GROUP_XML .= '<fields_note_'.$k.$i.'>
                                                    <frontend_type>note</frontend_type>
                                                    <comment>
                                                        <![CDATA[Enter names for each of the custom <span class="notice">'.$fieldType.'</span> fields.]]>
                                                    </comment>
                                                    <sort_order>'.$x++.'</sort_order>
                                                    <show_in_default>1</show_in_default>
                                                    <show_in_website>1</show_in_website>
                                                    <show_in_store>1</show_in_store>
                                                </fields_note_'.$k.$i.'>';
                            }
                        }


                    }


                     $GROUP_XML .= '</fields>
                            </mmd_cf_name_'.$pageAlias.'>';

            $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
            $adminSectionGroups->appendChild($new_group_xml);

            $i++;   
        }        
 
        return $this;
    }

    public function mmd_customfields_content(Varien_Event_Observer $observer)
    {

        $helper = Mage::helper('mmd_customfields');

        $config     = $observer->getConfig();
        $cmsPages   = Mage::getModel('cms/page')->getCollection();

        $fieldTypes = array(
            'text_fields_'      => 'text',
            'textarea_fields_'  => 'textarea',
            'wysiwyg_fields_'   => 'editor',
            'image_fields_'     => 'image',
            'yn_fields_'        => 'select',
            'ed_fields_'        => 'select'
        );

        $GROUP_XML  = '';

        $adminSectionGroups = $config->getNode('sections/mmd_customfields_content/groups');

        $GROUP_XML = '<mmd_cf_content_global>
                        <label>Global (Header/Footer)</label>
                        <sort_order>0</sort_order>
                        <show_in_default>1</show_in_default>
                        <show_in_website>1</show_in_website>
                        <show_in_store>1</show_in_store>
                        <fields>';
                            
            $showNotice = false;
            $x = 0;
            foreach($fieldTypes as $k => $fieldType){

                $fieldCount = Mage::getStoreConfig('mmd_customfields_add/mmd_cf_add_global/'.$k.'0');

                for($fields=1;$fields<=$fieldCount;$fields++){

                    $fieldName = Mage::getStoreConfig('mmd_customfields_name/mmd_cf_name_global/field_global_'.$k.$fields);
                    $fieldAlias = $helper->aliasify($fieldName);

                    if(strlen($fieldName)>0){

                    $GROUP_XML .= '<field_global_'.$k.$fields.'>
                                        <label>
                                            <![CDATA[
                                            <table>
                                                <tr><td><strong>Name: </strong></td><td>'.$fieldName.'</td></tr>
                                                <tr><td><strong>Alias: </strong></td><td> '.$fieldAlias.'</td></tr>
                                            </table>
                                            ]]>
                                        </label>
                                        <frontend_type>'.$fieldType.'</frontend_type>';

                                        if($k=='wysiwyg_fields_'){ $GROUP_XML .= '<frontend_model>Mmd_CustomFields_block_adminhtml_system_config_editor</frontend_model>'; }

                                        
                                        if($k=='image_fields_'){ $GROUP_XML .= '<backend_model>mmd_customfields_model_system_config_backend_image_upload</backend_model>'; }

                                        if($k=='yn_fields_'){ $GROUP_XML .= '<source_model>adminhtml/system_config_source_yesno</source_model>'; }

                                        if($k=='ed_fields_'){ $GROUP_XML .= '<source_model>adminhtml/system_config_source_enabledisable</source_model>'; }


                                        $GROUP_XML .= '<sort_order>'.$x.'</sort_order>
                                        <show_in_default>1</show_in_default>
                                        <show_in_website>1</show_in_website>
                                        <show_in_store>1</show_in_store>
                                    </field_global_'.$k.$fields.'>';

                    }

                    $x++;
                }

                if(isset($fieldCount)){
                    if(intval($fieldCount)!=0){
                        $x++;
                        $showNotice = true;
                    }else{
                        $showNotice = false;
                    }
                }

            }

            if($showNotice){
                $GROUP_XML .= '<fields_note_0>
                                    <frontend_type>note</frontend_type>
                                    <comment>
                                        <![CDATA[Use the field <span class="notice">alias</span> to call the data on the frontend.]]>
                                    </comment>
                                    <sort_order>'.$x.'</sort_order>
                                    <show_in_default>1</show_in_default>
                                    <show_in_website>1</show_in_website>
                                    <show_in_store>1</show_in_store>
                                </fields_note_0>';
            }


            $GROUP_XML .= '</fields>
                    </mmd_cf_content_global>';

        $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
        $adminSectionGroups->appendChild($new_group_xml);

        $i = 1;
        $x = 0;
        foreach($cmsPages as $p){

            $pageId     = $p->getData('page_id');
            $pageTitle  = $p->getData('title');
            $pageAlias  = $helper->aliasify($pageTitle);

            $fieldAlia = array('global');

            $GROUP_XML = '<mmd_cf_content_'.$pageAlias.'>
                                <label>'.$pageTitle.'</label>
                                <sort_order>'.$i.'</sort_order>
                                <show_in_default>1</show_in_default>
                                <show_in_website>1</show_in_website>
                                <show_in_store>1</show_in_store>
                                <fields>';

                    foreach($fieldTypes as $k => $fieldType){

                        $fieldCount = Mage::getStoreConfig('mmd_customfields_add/mmd_cf_add_'.$pageAlias.'/'.$k.$i);

                        for($fields=1;$fields<=$fieldCount;$fields++){

                            $fieldName = Mage::getStoreConfig('mmd_customfields_name/mmd_cf_name_'.$pageAlias.'/field_'.$pageAlias.'_'.$k.$fields);
                            $fieldAlias = $helper->aliasify($fieldName);

                            if(strlen($fieldName)>0){

                                if(!in_array($fieldAlias, $fieldAlia)){
                                    
                                    $fieldAlia[] = $fieldAlias;

                                    $GROUP_XML .= '<field_'.$pageAlias.'_'.$fieldAlias.'>
                                                        <label>
                                                            <![CDATA[
                                                            <table>
                                                                <tr><td><strong>Name: </strong></td><td>'.$fieldName.'</td></tr>
                                                                <tr><td><strong>Alias: </strong></td><td> '.$fieldAlias.'</td></tr>
                                                            </table>
                                                            ]]>
                                                        </label>
                                                        <frontend_type>'.$fieldType.'</frontend_type>';

                                                        if($k=='wysiwyg_fields_'){
                                                            $GROUP_XML .= '<frontend_model>Mmd_CustomFields_block_adminhtml_system_config_editor</frontend_model>';
                                                        }

                                                        
                                                        if($k=='image_fields_'){
                                                            $GROUP_XML .= '<backend_model>mmd_customfields_model_system_config_backend_image_upload</backend_model>';
                                                        }

                                                        if($k=='yn_fields_'){
                                                            $GROUP_XML .= '<source_model>adminhtml/system_config_source_yesno</source_model>';
                                                        }

                                                        if($k=='ed_fields_'){
                                                            $GROUP_XML .= '<source_model>adminhtml/system_config_source_enabledisable</source_model>';
                                                        }


                                                        $GROUP_XML .= '<sort_order>'.$x.'</sort_order>
                                                        <show_in_default>1</show_in_default>
                                                        <show_in_website>1</show_in_website>
                                                        <show_in_store>1</show_in_store>
                                                    </field_'.$pageAlias.'_'.$fieldAlias.'>';
                                }else{
                                    $GROUP_XML .= '<fields_note_'.$x.'_'.$fieldAlias.'>
                                                        <frontend_type>note</frontend_type>
                                                        <comment>
                                                            <![CDATA[<strong><span class="notice">'.$fieldName.'</span></strong> is a duplicate. <strong>Rename one!</strong>]]>
                                                        </comment>
                                                        <sort_order>'.$x.'</sort_order>
                                                        <show_in_default>1</show_in_default>
                                                        <show_in_website>1</show_in_website>
                                                        <show_in_store>1</show_in_store>
                                                    </fields_note_'.$x.'_'.$fieldAlias.'>';
                                }

                            }

                            $x++;
                        }

                        if(isset($fieldCount)){
                            if(intval($fieldCount)!=0){
                                $x++;
                                $showNotice = true;
                            }else{
                                $showNotice = false;
                            }
                        }

                    }

                    if($showNotice){
                        $GROUP_XML .= '<fields_note_'.$i.'>
                                            <frontend_type>note</frontend_type>
                                            <comment>
                                                <![CDATA[Use the field <span class="notice">alias</span> to call the data on the frontend.]]>
                                            </comment>
                                            <sort_order>'.$x.'</sort_order>
                                            <show_in_default>1</show_in_default>
                                            <show_in_website>1</show_in_website>
                                            <show_in_store>1</show_in_store>
                                        </fields_note_'.$i.'>';
                    }


                    $GROUP_XML .= '</fields>
                            </mmd_cf_content_'.$pageAlias.'>';

            $new_group_xml = new Mage_Core_Model_Config_Element($GROUP_XML);
            $adminSectionGroups->appendChild($new_group_xml);

            $i++;   
        }        
 
        return $this;
    }

    public function __p($array=array())
    {
    	echo "<pre>";
        var_dump($array);
		echo "</pre>";
    }

    public function __log($var="empty")
    {
    	#echo "<pre>";
        #var_dump($array);
		#echo "</pre>";
		
		if(is_array($var)){
			$result = implode(' -- ', $var);
		}else{
			$result = $var;
		}

		echo '<script type="text/javascript">console.log("'.$result.'");</script>';
    }

}