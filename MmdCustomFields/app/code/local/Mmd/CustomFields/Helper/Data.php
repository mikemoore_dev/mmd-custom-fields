<?php
class Mmd_CustomFields_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function getGlobalField($fieldAlias)
	{
		return Mage::getStoreConfig('mmd_customfields_content/mmd_cf_content_global/field_global_'.$fieldAlias);
	}

	public function getPageField($fieldAlias)
	{
		$pageName = Mage::getSingleton('cms/page')->getTitle();
		$pageAlias = $this->aliasify($pageName);
		return Mage::getStoreConfig('mmd_customfields_content/mmd_cf_content_'.$pageAlias.'/field_'.$pageAlias.'_'.$fieldAlias);
	}

	public function aliasify($var)
    {
        $var = preg_replace("/[^A-Za-z0-9 ]/", '', $var);
        $var = str_replace(' ','_',$var);
        $var = strtolower($var);
        return $var;
    }

}